# README #

This README document whatever steps are necessary to get the application up and running.

### 1. Build the application artifact. ###
* mvn clean install

### 2. Run the application. ###
* java --enable-preview -jar target/db-drawing-1.0.jar

### 3. Run the tests. ###
* mvn test
