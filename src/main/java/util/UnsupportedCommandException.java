package util;

public class UnsupportedCommandException extends RuntimeException{

    private String message;

    public UnsupportedCommandException(String message) {
        super(message);
    }
}
