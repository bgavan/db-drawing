package util;

public class Constant {
    public static final int MARGIN_COUNT = 2;
    public static final char EMPTY = ' ';
    public static final char HORIZONTAL = '-';
    public static final char VERTICAL = '|';
    public static final char X_CHAR = 'x';
}
