package util;

import shape.Graphics;
import shape.Point;

public class Utils {

    public static boolean isInsideCanvas(Point point, Graphics graphics) {
        if(point.getX() > graphics.getWidth() || point.getX() < 1)
            throw new IllegalArgumentException("Coordinate x = " + point.getX() + " is outside of canvas");
        if(point.getY() > graphics.getHeight() || point.getY() < 1)
            throw new IllegalArgumentException("Coordinate y = " + point.getY() + " is outside of canvas");

        return true;
    }
}
