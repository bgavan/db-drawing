package util;

public class UnsupportedGraphicsException extends RuntimeException {

    private String message;

    public UnsupportedGraphicsException(String message) {
        super(message);
    }
}
