import helper.Helper;
import helper.HelperFactory;
import shape.Graphics;
import shape.Shape;
import shape.ShapeFactory;
import util.UnsupportedCommandException;
import util.UnsupportedGraphicsException;

import java.util.Scanner;

public class Application {

    private static Graphics graphics;
    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        graphics = new Graphics();
        System.out.print("enter command: ");
        while (true) {
            process(scanner.nextLine());
            System.out.print("enter command: ");
        }
    }

    private static void process(String commandLine) {
        try{
            Helper helper = HelperFactory.getHelper(commandLine);
            Shape shape = ShapeFactory.getShape(helper);

            if(shape != null) {
                shape.draw(graphics);
            } else {
                scanner.close();
                System.out.println("Console will be closed.");
                System.exit(0);
            }
        }catch (UnsupportedCommandException uce) {
            System.out.println(uce.getMessage());
        }catch (UnsupportedGraphicsException uge) {
            System.out.println(uge.getMessage());
        } catch (NumberFormatException nfe) {
            System.out.println(nfe.getMessage() + "Please insert a valid number");
        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }
}
