package shape;

public interface Shape {
    void draw(Graphics canvas);
}
