package shape;

import java.util.Arrays;

public class Graphics {
    private char[][] canvasPoints;
    private int height;
    private int width;
    public Graphics(){}

    public void display() {
        Arrays.stream(canvasPoints).forEach(System.out::println);
    }

    public void setCanvasPoints(char[][] canvasPoints) {
        this.canvasPoints = canvasPoints;
    }

    public char[][] getCanvasPoints() {
        return canvasPoints;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
