package shape;

import util.UnsupportedGraphicsException;

import java.util.Objects;

import static util.Constant.X_CHAR;
import static util.Utils.isInsideCanvas;

public final class Rectangle implements Shape {

    private final Point upperLeft;
    private final Point lowerRight;

    public Rectangle (Point upperLeft, Point lowerRight) {
        this.checkCoordinates(upperLeft, lowerRight);
        this.upperLeft = upperLeft;
        this.lowerRight = lowerRight;
    }

    public Point getUpperLeft() {
        return upperLeft;
    }

    public Point getLowerRight() {
        return lowerRight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Objects.equals(upperLeft, rectangle.upperLeft) &&
                Objects.equals(lowerRight, rectangle.lowerRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(upperLeft, lowerRight);
    }

    @Override
    public void draw(Graphics graphics) {
        var canvasPoints = graphics.getCanvasPoints();
        if(Objects.isNull(canvasPoints)) {
            throw new UnsupportedGraphicsException("Canvas is not ready. Please create canvas first.");
        }

        if(isInsideCanvas(this.upperLeft, graphics) && isInsideCanvas(this.lowerRight, graphics)) {

            for(int i = this.getUpperLeft().getX(); i <= this.getLowerRight().getX(); i++) {
                canvasPoints[this.getUpperLeft().getY()][i] = X_CHAR;
                canvasPoints[this.getLowerRight().getY()][i] = X_CHAR;
            }
            for(int i = this.getUpperLeft().getY() + 1; i < this.getLowerRight().getY(); i++) {
                canvasPoints[i][this.getUpperLeft().getX()] = X_CHAR;
                canvasPoints[i][this.getLowerRight().getX()] = X_CHAR;
            }

            graphics.display();
        }
    }

    private void checkCoordinates(Point upperLeft, Point lowerRight) {
        if(upperLeft.getX() > lowerRight.getX() || upperLeft.getY() > lowerRight.getY())
            throw new IllegalArgumentException("Lower right coordinates can not have lower values as the upper right");
    }
}
