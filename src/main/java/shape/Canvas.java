package shape;

import java.util.Objects;

import static util.Constant.EMPTY;
import static util.Constant.HORIZONTAL;
import static util.Constant.MARGIN_COUNT;
import static util.Constant.VERTICAL;

public final class Canvas implements Shape {

    private final int height;
    private final int width;

    public Canvas(int width, int height) {
        validateParams(width, height);
        this.width = width;
        this.height = height;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Canvas canvas = (Canvas) o;
        return height == canvas.height &&
                width == canvas.width;
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, width);
    }

    private void validateParams(int x, int y) {
        if(x < 0 || y < 0) throw new IllegalArgumentException("Arguments should be positive.");
    }

    @Override
    public void draw(Graphics graphics) {
        var canvasPoints = new char[height + MARGIN_COUNT][width + MARGIN_COUNT];
        for(int i = 0; i < height + MARGIN_COUNT; i++) {
            for (int j =0; j < width + MARGIN_COUNT; j++) {
                if(i == 0 || i == height + MARGIN_COUNT-1) {
                    canvasPoints[i][j] = HORIZONTAL;
                }else if(j == 0 || j == width + MARGIN_COUNT -1) {
                    canvasPoints[i][j] = VERTICAL;
                }else{
                    canvasPoints[i][j] = EMPTY;
                }
            }
        }

        graphics.setCanvasPoints(canvasPoints);
        graphics.setWidth(width);
        graphics.setHeight(height);
        graphics.display();
    }
}
