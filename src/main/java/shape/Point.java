package shape;


import java.util.Objects;

public class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        validatePosition(x, y);
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    private void validatePosition(int x, int y) {
        if(x < 0 || y < 0) throw new IllegalArgumentException("Coordinates should be positive.");
    }
}
