package shape;

import util.UnsupportedGraphicsException;

import java.util.Objects;
import static util.Constant.X_CHAR;
import static util.Utils.isInsideCanvas;

public final class Line implements Shape {

    private final Point c1;
    private final Point c2;

    public Line(final Point c1, final Point c2) {
        validateCoordinates(c1, c2);
        this.c1 = c1;
        this.c2 = c2;
    }

    public Point getC1() {
        return c1;
    }
    public Point getC2() {
        return c2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return Objects.equals(c1, line.c1) &&
                Objects.equals(c2, line.c2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(c1, c2);
    }

    public boolean isHorizontal() {
        return c1.getY() == c2.getY();
    }

    @Override
    public void draw(Graphics graphics) {
        var canvasPoints =  graphics.getCanvasPoints();
        if(Objects.isNull(canvasPoints)) {
            throw new UnsupportedGraphicsException("Canvas is not ready. Please create canvas first.");
        }

        if(isInsideCanvas(this.c1, graphics) && isInsideCanvas(this.c2, graphics)){

            if(isHorizontal()) {
                for(int i = this.getC1().getX(); i <= this.getC2().getX();i++) {
                    canvasPoints[this.getC1().getY()][i] = X_CHAR;
                }
            }else {
                for(int i = this.getC1().getY(); i <= this.getC2().getY(); i++) {
                    canvasPoints[i][this.getC1().getX()] = X_CHAR;
                }
            }
            graphics.display();
        }
    }

    private void validateCoordinates(Point c1, Point c2) {
        if(c1.getX() != c2.getX() && c1.getY() != c2.getY())
            throw new IllegalArgumentException("Coordinates should be on the same line, either vertically, either horizontally.");
    }
}
