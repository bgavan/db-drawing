package shape;

import helper.ExitHelper;
import helper.Helper;
import helper.HelperCanvas;
import helper.LineHelper;
import helper.RectangleHelper;

public class ShapeFactory {

    public static Shape getShape(Helper objectHelper) {
        if(objectHelper instanceof HelperCanvas helperCanvas) {
            return new Canvas(helperCanvas.getWidth(), helperCanvas.getHeight());
        }

        if(objectHelper instanceof LineHelper lineHelper) {
            return new Line(lineHelper.getC1(), lineHelper.getC2());
        }

        if(objectHelper instanceof RectangleHelper rectangleHelper) {
            return new Rectangle(rectangleHelper.getC1(), rectangleHelper.getC2());
        }
        if(objectHelper instanceof ExitHelper exitHelper) {
            return null;
        }
        return null;
    }
}
