package helper;

public class HelperCanvas implements Helper {

    private final int width;
    private final int height;
    public HelperCanvas(String[] params) {
        if(params.length < 2) throw new IllegalArgumentException("Too few parameters. You should use: C w h");
        width = Integer.valueOf(params[0]);
        height = Integer.valueOf(params[1]);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
