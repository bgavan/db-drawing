package helper;


import java.util.Arrays;

public class HelperFactory {

    private static final String EMPTY_REGEX = " ";

    public static Helper getHelper(String commandLine) {
        String[] commandParts = commandLine.split(EMPTY_REGEX);
        String command = commandParts[0].toUpperCase();
        String[] params = Arrays.copyOfRange(commandParts, 1, commandParts.length);

        return switch (CommandLine.valueOf(command)) {
            case C -> new HelperCanvas(params);
            case L -> new LineHelper(params);
            case R -> new RectangleHelper(params);
            case Q -> new ExitHelper();
        };
    }

    enum CommandLine {
        C, L, R, Q
    }
}
