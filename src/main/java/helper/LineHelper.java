package helper;

import shape.Point;
import static java.lang.Integer.parseInt;

public class LineHelper implements Helper {
    private final Point c1;
    private final Point c2;
    public LineHelper(String[] params) {
        if(params.length < 4) throw new IllegalArgumentException("Too few arguments. You should use L x1 y1 x2 y2");
        c1 = new Point(parseInt(params[0]), parseInt(params[1]));
        c2 = new Point(parseInt(params[2]), parseInt(params[3]));
    }

    public Point getC1() {
        return c1;
    }

    public Point getC2() {
        return c2;
    }
}
