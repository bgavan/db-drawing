package helper;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HelperFactoryTest {

    @Test
    public void createCanvasHelper () {
        Helper helper = HelperFactory.getHelper("C 20 5");
        assertTrue(helper instanceof HelperCanvas);
    }

    @Test
    public void createLineHelper () {
        Helper helper = HelperFactory.getHelper("L 1 1 1 1");
        assertTrue(helper instanceof LineHelper);
    }

    @Test
    public void createRectangleHelper () {
        Helper helper = HelperFactory.getHelper("R 1 1 1 1");
        assertTrue(helper instanceof RectangleHelper);
    }

    @Test
    public void createExitHelper () {
        Helper helper = HelperFactory.getHelper("Q");
        assertTrue(helper instanceof ExitHelper);
    }
}
