package helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LineHelperTest {

    @Test
    public void createLineHelper() {
        var params = new String[] { "1", "1", "1", "1"};
        LineHelper lineHelper = new LineHelper(params);

        assertEquals(1, lineHelper.getC1().getX());
        assertEquals(1, lineHelper.getC1().getY());
        assertEquals(1, lineHelper.getC2().getX());
        assertEquals(1, lineHelper.getC2().getY());
    }

    @Test
    public void testLineHelperWithFewParameters() {
        var params = new String[] {"20"};
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> new LineHelper(params));
        assertEquals("Too few arguments. You should use L x1 y1 x2 y2", throwable.getMessage());
    }

    @Test
    public void testLineHelperWithInvalidParam1() {
        var params = new String[] {"a", "1", "1", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new LineHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testLineHelperWithInvalidParam2() {
        var params = new String[] {"1", "a", "1", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new LineHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testLineHelperWithInvalidParam3() {
        var params = new String[] {"1", "1", "a", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new LineHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testLineHelperWithInvalidParam4() {
        var params = new String[] {"1", "1", "1", "a"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new LineHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

}
