package helper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class HelperCanvasTest {

    @Test
    public void testHelperCanvas () {
        var params = new String[] {"20", "5"};
        HelperCanvas helperCanvas = new HelperCanvas(params);
        assertEquals(20, helperCanvas.getWidth());
        assertEquals(5, helperCanvas.getHeight());
    }

    @Test
    public void testHelperCanvasWithFewParameters() {
        var params = new String[] {"20"};
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> new HelperCanvas(params));
        assertEquals("Too few parameters. You should use: C w h", throwable.getMessage());
    }

    @Test
    public void testHelperCanvasWithInvalidWidth() {
        var params = new String[] {"a", "5"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new HelperCanvas(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testHelperCanvasWithInvalidHeight() {
        var params = new String[] {"20", "a"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new HelperCanvas(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }
}
