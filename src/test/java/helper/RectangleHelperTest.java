package helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RectangleHelperTest {
    @Test
    public void createRectangleHelper() {
        var params = new String[] { "1", "1", "1", "1"};
        RectangleHelper RectangleHelper = new RectangleHelper(params);

        assertEquals(1, RectangleHelper.getC1().getX());
        assertEquals(1, RectangleHelper.getC1().getY());
        assertEquals(1, RectangleHelper.getC2().getX());
        assertEquals(1, RectangleHelper.getC2().getY());
    }

    @Test
    public void testRectangleHelperWithFewParameters() {
        var params = new String[] {"20"};
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> new RectangleHelper(params));
        assertEquals("Too few arguments. You should use R x1 y1 x2 y2", throwable.getMessage());
    }

    @Test
    public void testRectangleHelperWithInvalidParam1() {
        var params = new String[] {"a", "1", "1", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new RectangleHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testRectangleHelperWithInvalidParam2() {
        var params = new String[] {"1", "a", "1", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new RectangleHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testRectangleHelperWithInvalidParam3() {
        var params = new String[] {"1", "1", "a", "1"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new RectangleHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

    @Test
    public void testRectangleHelperWithInvalidParam4() {
        var params = new String[] {"1", "1", "1", "a"};
        Throwable throwable = assertThrows(NumberFormatException.class, () -> new RectangleHelper(params));
        assertEquals("For input string: \"a\"", throwable.getMessage());
    }

}
