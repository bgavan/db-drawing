package shape;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PointTest {

    @Test
    public void testCreatePoint() {
        Point point =  new Point(1, 2);
        assertNotNull(point);
        assertEquals(1, point.getX());
        assertEquals(2, point.getY());
    }

    @Test
    public void testCreatePointWithXNegative() {
       Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Point(-1, 1));
       assertEquals(exception.getMessage(), "Coordinates should be positive.");
    }

    @Test
    public void testCreatePointWithYNegative() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Point(1, -1));
        assertEquals(exception.getMessage(), "Coordinates should be positive.");
    }

    @Test
    public void testCreatePointWithXAndYNegative() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Point(-1, -1));
        assertEquals(exception.getMessage(), "Coordinates should be positive.");
    }
}
