package shape;

import helper.Helper;
import helper.HelperFactory;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ShapeFactoryTest {

    @Test
    public void testCreateCanvas() {
        Helper helper = HelperFactory.getHelper("C 2 3");
        Shape shape = ShapeFactory.getShape(helper);
        assertTrue(shape instanceof Canvas);
    }

    @Test
    public void testCreateLine() {
        Helper helper = HelperFactory.getHelper("L 1 1 1 1");
        Shape shape = ShapeFactory.getShape(helper);
        assertTrue(shape instanceof Line);
    }

    @Test
    public void testCreateRectangle() {
        Helper helper = HelperFactory.getHelper("R 1 1 1 1");
        Shape shape = ShapeFactory.getShape(helper);
        assertTrue(shape instanceof Rectangle);
    }

    @Test
    public void testCreateNullObject() {
        Helper helper = HelperFactory.getHelper("Q");
        Shape shape = ShapeFactory.getShape(helper);
        assertNull(shape);
    }
}
