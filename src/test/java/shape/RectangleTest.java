package shape;

import org.junit.jupiter.api.Test;
import util.UnsupportedGraphicsException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {

    @Test
    public void testCreateRectangle(){
        Rectangle rectangle = new Rectangle(new Point( 15, 2), new Point(20, 5));
        assertNotNull(rectangle);
        assertEquals(15, rectangle.getUpperLeft().getX());
        assertEquals(2, rectangle.getUpperLeft().getY());
        assertEquals(20, rectangle.getLowerRight().getX());
        assertEquals(5, rectangle.getLowerRight().getY());
    }

    @Test
    public void testCreateRectangleWithXInvalid() {
        Throwable throwable = assertThrows(IllegalArgumentException.class,
                () -> new Rectangle(new Point(20, 2), new Point(15, 5)));
        assertEquals(throwable.getMessage(), "Lower right coordinates can not have lower values as the upper right");
    }

    @Test
    public void testCreateRectangleWithYInvalid() {
        Throwable throwable = assertThrows(IllegalArgumentException.class,
                () -> new Rectangle(new Point(15, 5), new Point(20, 2)));
        assertEquals(throwable.getMessage(), "Lower right coordinates can not have lower values as the upper right");
    }

    @Test
    public void testCreateRectangleWithXYInvalid() {
        Throwable throwable = assertThrows(IllegalArgumentException.class,
                () -> new Rectangle(new Point(20, 5), new Point(15, 2)));
        assertEquals(throwable.getMessage(), "Lower right coordinates can not have lower values as the upper right");
    }

    @Test
    public void testDrawRectangle() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', 'x', 'x', 'x', '|'},
                {'|', 'x', ' ', 'x', '|'},
                {'|', 'x', 'x', 'x', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Canvas canvas = new Canvas(3, 3);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);
        Rectangle rectangle = new Rectangle(new Point(1,1), new Point(3,3));
        rectangle.draw(graphics);

        assertArrayEquals(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawRectangleWithoutCanvas() {
        Graphics graphics = new Graphics();
        Rectangle rectangle = new Rectangle(new Point(1,1), new Point(3,3));
        Throwable exception = assertThrows(UnsupportedGraphicsException.class, () -> rectangle.draw(graphics));
        assertEquals(exception.getMessage(), "Canvas is not ready. Please create canvas first.");
    }
}
