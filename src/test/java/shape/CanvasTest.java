package shape;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class CanvasTest {

    @Test
    public void testCreateCanvas() {
        Canvas canvas = new Canvas(1, 1);
        assertNotNull(canvas);
    }

    @Test
    public void testCreateCanvasWithWidthNegative() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Canvas(-1, 1));
        assertEquals(exception.getMessage(), "Arguments should be positive.");
    }

    @Test
    public void testCreateCanvasWithHeightNegative() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Canvas(1, -1));
        assertEquals(exception.getMessage(), "Arguments should be positive.");
    }

    @Test
    public void testCreateCanvasWithWidthAndHeightNegative() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new Canvas(-1, -1));
        assertEquals(exception.getMessage(), "Arguments should be positive.");
    }

    @Test
    public void testDrawCanvas() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Canvas canvas = new Canvas(3, 3);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);

        assertArrayEquals(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawCanvasWrongWidth() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Canvas canvas = new Canvas(4, 3);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);

        assertNotSame(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawCanvasWrongHeight() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Canvas canvas = new Canvas(3, 4);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);

        assertNotSame(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawCanvasWithWrongWidthAndHeight() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Canvas canvas = new Canvas(4, 4);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);

        assertNotSame(expectedCanvas, graphics.getCanvasPoints());
    }
}
