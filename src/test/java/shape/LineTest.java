package shape;

import org.junit.jupiter.api.Test;
import util.UnsupportedGraphicsException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LineTest {

    @Test
    public void testCreateLineHorizontally() {
        Line line = new Line(new Point(1,3), new Point(7,3));
        assertNotNull(line);
        assertEquals(1, line.getC1().getX());
        assertEquals(3, line.getC1().getY());
        assertEquals(7, line.getC2().getX());
        assertEquals(3, line.getC2().getY());

    }

    @Test
    public void testCreateLineVertically() {
        Line line = new Line(new Point(7,1), new Point(7,3));
        assertNotNull(line);
        assertEquals(7, line.getC1().getX());
        assertEquals(1, line.getC1().getY());
        assertEquals(7, line.getC2().getX());
        assertEquals(3, line.getC2().getY());

    }

    @Test
    public void testCreateLineWithInvalidArguments() {
        Throwable exception = assertThrows(IllegalArgumentException.class,
                () -> new Line(new Point(1, 2), new Point(3, 4)));
        assertEquals(exception.getMessage(),
                "Coordinates should be on the same line, either vertically, either horizontally.");
    }

    @Test
    public void testDrawLineHorizontally() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', ' ', ' ', ' ', '|'},
                {'|', 'x', 'x', 'x', '|'},
                {'|', ' ', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Line line = new Line(new Point(1,2), new Point(3, 2));
        Canvas canvas = new Canvas(3, 3);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);
        line.draw(graphics);

        assertArrayEquals(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawLineVertically() {
        var expectedCanvas = new char[][] {
                {'-', '-', '-', '-', '-'},
                {'|', 'x', ' ', ' ', '|'},
                {'|', 'x', ' ', ' ', '|'},
                {'|', 'x', ' ', ' ', '|'},
                {'-', '-', '-', '-', '-'},
        };

        Line line = new Line(new Point(1,1), new Point(1, 3));
        Canvas canvas = new Canvas(3, 3);
        Graphics graphics = new Graphics();
        canvas.draw(graphics);
        line.draw(graphics);

        assertArrayEquals(expectedCanvas, graphics.getCanvasPoints());
    }

    @Test
    public void testDrawLineWithoutCanvas() {
        Line line = new Line(new Point(1,1), new Point(1, 3));
        Graphics graphics = new Graphics();
        Throwable throwable = assertThrows(UnsupportedGraphicsException.class, () -> line.draw(graphics));
        assertEquals(throwable.getMessage(), "Canvas is not ready. Please create canvas first.");
    }
}


