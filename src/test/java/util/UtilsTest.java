package util;

import org.junit.jupiter.api.BeforeAll;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import shape.Canvas;
import shape.Graphics;
import shape.Point;

public class UtilsTest {

    private static Graphics graphics;

    @BeforeAll
    public static void setup() {
        graphics = new Graphics();
        Canvas canvas = new Canvas(20, 5);
        canvas.draw(graphics);
    }

    @Test
    public void testIsInside() {
        Point point = new Point(1, 1);
        assertTrue(Utils.isInsideCanvas(point, graphics));
    }

    @Test
    public void testIsOutsideWithX() {
        Point point = new Point(22, 1);
        var message = "Coordinate x = 22 is outside of canvas";
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> Utils.isInsideCanvas(point, graphics));
        assertEquals(message, throwable.getMessage());
    }

    @Test
    public void testIsOutsideWithXZero() {
        Point point = new Point(0, 1);
        var message = "Coordinate x = 0 is outside of canvas";
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> Utils.isInsideCanvas(point, graphics));
        assertEquals(message, throwable.getMessage());
    }

    @Test
    public void testIsOutsideWithY() {
        Point point = new Point(1, 7);
        var message = "Coordinate y = 7 is outside of canvas";
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> Utils.isInsideCanvas(point, graphics));
        assertEquals(message, throwable.getMessage());
    }

    @Test
    public void testIsOutsideWithYZero() {
        Point point = new Point(1, 0);
        var message = "Coordinate y = 0 is outside of canvas";
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> Utils.isInsideCanvas(point, graphics));
        assertEquals(message, throwable.getMessage());
    }
}
